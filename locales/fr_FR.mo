��    2      �  C   <      H     I     P  F   p  '   �     �     �          !  '   7     _  #   y     �  #   �     �  -   �       "   +     N  	   n     x     �     �     �     �     �     �     �       <     %   N  /   t  %   �  :   �  !        '     =  0   T     �  *   �  &   �     �     	     %	     @	  %   ]	  +   �	  (   �	  1   �	  ,   

  �  7
     �     �  B   �  1   >     p     �     �     �  2   �  %     ,   6     c  )   v     �  9   �  ,   �  )     +   A     m          �     �     �     �  "   �     	          1  C   5  .   y  3   �  .   �  >     !   J     l     �  -   �     �  3   �  2     $   @  !   e  #   �  "   �  ,   �  ,   �  (   (  5   Q  1   �     -                           *   &      '       $   "   	          ,   /           .   !      1              
                   2              )      %              0                (   #                            +                     %name% %user% (%type_name%):
%content% An approval request has been submitted by %user%:
%comment_submission% Approval request on ticket #%ticket_id% Approval requests Browser Notification Default notification sound Ignore deleted items? New assignment in ticket (#%ticket_id%) New assignment in tickets New document on ticket #%ticket_id% New documents New followup on ticket #%ticket_id% New followups New group assignment in ticket (#%ticket_id%) New group assignment in tickets New task (%state_text%):
%content% New task on ticket #%ticket_id% New tasks New ticket #%ticket_id% New tickets Notification sound Scheduled Tasks Scheduled task Show an example notification Show example Show notifications Sound Status of #%ticket_id% is changed to
%status%
by %user_name% Status updated on ticket #%ticket_id% Task scheduled for %datetime_format%:
%content% Task scheduled on ticket #%ticket_id% The document "%filename%" has added on ticket #%ticket_id% This plugin requires GLPI >= 0.85 Ticket status updated Tickets status updated Time to check for new notifications (in seconds) URL of the icon You assigned to ticket #%ticket_id%
%name% You have %count% new approval requests You have %count% new documents You have %count% new followups You have %count% new tasks You have %count% new tickets You have %count% new tickets assigned You have %count% new tickets status updated You have %count% scheduled tasks for now Your group assigned to ticket #%ticket_id%
%name% Your group have %count% new tickets assigned Project-Id-Version: 
POT-Creation-Date: 2017-04-19 17:48+0200
PO-Revision-Date: 2017-04-19 18:03+0200
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.1
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: __bn
X-Poedit-SourceCharset: UTF-8
Last-Translator: 
Language: fr
X-Poedit-SearchPath-0: ..
 %name% %user% (%type_name%):
%content% une demande d'approbation soumise par %user%:
%comment_submission% Demande d'approbation pour le ticket #%ticket_id% Demande d'approbation Notification navigateur Son de notification par défaut Ignorer élément supprimés ? Nouvelle assignation dans le ticket (#%ticket_id%) Nouvelle assignation dans les tickets Nouveau document pour le ticket #%ticket_id% Nouveaux documents Nouveau suivi pour le ticket #%ticket_id% Nouveaux suivis Nouveau groupe d'assignation pour le ticket(#%ticket_id%) Nouveau group d'assignation dans les tickets Nouvelle tâche (%state_text%):
%content% Nouvelle tâche pour le ticket #%ticket_id% Nouvelles tâches Nouveau ticket #%ticket_id% Nouveaux tickets Son de notification Tâches planifiées Tâche planifiée Afficher un exempe de notification Afficher exemple Afficher notifications Son Statut du ticket  #%ticket_id%  modifié en
%status%
by %user_name% Statut mis à jour pour le ticket #%ticket_id% tâche planifiée pour %datetime_format%:
%content% Tâche planifiée pour le tickett #%ticket_id% Le document "%filename%" a été ajouté a ticket #%ticket_id% Le plugin nécessite GLPI >= 0.85 MAJ du statut du ticket MAJ du statut du ticket Temps verification notification (en secondes) URL de l'icone Vous êtes assigné au ticket (#%ticket_id%)
%name% Vous avez %count% nouvelles demandes d'approbation Vous avez %count% nouveaux documents Vous avez %count% nouveaux suivis Vous avez %count% nouvelles tâches Vous avez %count% nouveaux tickets Vous avez %count% nouveaux tickets assignés Vous avez %count% nouveaux tickets assignés You have %count% scheduled tasks for now ticket #%ticket_id%  assigné à votre groupe 
%name% votre groupe a %count% nouveaux tickets assignés 